# Caesar Cipher

import sys

# User input text
print("Start")


def main():
    if len(sys.argv) == 3:  # case with provided shift
        sys.argv[2]
        shift(sys.argv[1], int(sys.argv[2]))
    elif len(sys.argv) == 2:  # case with no shift
        sys.argv[1]
        rotateThrough(sys.argv[1])
        text = sys.argv[1]
    else:  # case with only .py file
        exit()


def shift(file, shift):
    text = open(file)  # sys.argv -> open(file) -> text -> text.read;
    # prints the text
    print("========================")
    print("Rotated by ", shift, "positions")
    print("========================")
    for c in text.read():
        n = ord(c)
        if 96 < n < 123:
            x = n + shift
            if x > 122:
                x = x - 26
            print(chr(x), end='')
        elif 64 < n < 91:
            x = n + shift
            if x > 90:
                x = x - 26
            print(chr(x), end='')
        else:
            print(c, end='')
    print('\n')


def rotateThrough(file):  #
    for i in range(26):
        shift(file, i)


main()  # executes program
