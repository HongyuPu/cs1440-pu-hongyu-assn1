# Caesar Cipher

import sys


def main():
    if len(sys.argv) == 3:  # case with provided shift
        if 26 > int(sys.argv[2]) > -1:
            rotate(sys.argv[1], int(sys.argv[2]))
        else:
            print("Error: rotation distance must be between 0 and 25")
    elif len(sys.argv) == 2:  # case with no shift
        rotateThrough(sys.argv[1])
    else:  # case with only .py file
        print("Usage: src/caesar.py FILENAME [ROTATION]", '\n')
        print("When no rotation distance is specified all 25 possible rotations are tried")
        exit()


def rotate(file, shift):
    text = open(file)  # sys.argv -> open(file) -> text -> text.read;
    # prints the text
    print("========================")
    print("Rotated by ", shift, "positions")
    print("========================")
    for c in text.read():  # only shifts letters
        n = ord(c)
        if 96 < n < 123:
            x = n + shift
            if x > 122:
                x = x - 26
            print(chr(x), end='')
        elif 64 < n < 91:
            x = n + shift
            if x > 90:
                x = x - 26
            print(chr(x), end='')
        else:
            print(c, end='')
    print('\n')


def rotateThrough(file):  # shifts through all possibilities
    for i in range(26):
        rotate(file, i)


main()  # executes program
